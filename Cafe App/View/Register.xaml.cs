using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        public Register()
        {
            InitializeComponent();
            vm = new RegisterViewModel();
            DataContext = vm;
        }

        private RegisterViewModel vm;
        private View.Login login;
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelRegister.password = Password.Password;
            await vm.CreateAkun();
            login = new View.Login();
            login.Show();
            Close();
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            login = new View.Login();
            login.Show();
            Close();
        }
    }
}
