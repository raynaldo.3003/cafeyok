using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
            ChangeColor(0);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ChangeColor(0);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Order1());
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            ChangeColor(1);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Laporan());
        }

        private void Grid_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            ChangeColor(2);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Produk());
        }

        private void ChangeColor(int index)
        {
            var click = (SolidColorBrush)(new BrushConverter().ConvertFrom("#80AC7533"));
            var noclick = (SolidColorBrush)(new BrushConverter().ConvertFrom("#00AC7533"));

            OrderMenu.Background = noclick;
            LaporanMenu.Background = noclick;
            ProdukMenu.Background = noclick;

            switch (index)
            {
                case 0:
                    OrderMenu.Background = click;
                    break;
                case 1:
                    LaporanMenu.Background = click;
                    break;
                case 2:
                    ProdukMenu.Background = click;
                    break;
            }
        }

        private View.Login login;
        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            login = new View.Login();
            login.Show();
            Close();
        }
    }
}
