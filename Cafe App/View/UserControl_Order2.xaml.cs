using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for UserControl_Order2.xaml
    /// </summary>
    public partial class UserControl_Order2 : UserControl
    {
        public UserControl_Order2(OrderViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void DatePickerTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Order1());
        }
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Order1());
        }
    }
}
