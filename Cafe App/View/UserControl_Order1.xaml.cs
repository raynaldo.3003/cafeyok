using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for UserControl_Order1.xaml
    /// </summary>
    public partial class UserControl_Order1 : UserControl
    {
        public UserControl_Order1()
        {
            InitializeComponent();
            vm = new OrderViewModel();
            DataContext = vm;
        }

        private OrderViewModel vm;
        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BarangSelect.Height = 90;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BarangSelect.Height = 0;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Order2(vm));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Order1());
        }
    }
}
