using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for UserControl_DetailBeli.xaml
    /// </summary>
    public partial class UserControl_DetailBeli : UserControl
    {
        public UserControl_DetailBeli(LaporanViewModel vm)
        {
            InitializeComponent();
            vm1 = vm;
            DataContext = vm;
        }
        LaporanViewModel vm1;
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await vm1.DeletePembelian();
            await Task.Delay(5);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Laporan());
        }
        private async void Button_Click1(object sender, RoutedEventArgs e)
        {
            await Task.Delay(0);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Laporan());
        }
    }
}
