using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            vm = new LoginViewModel();
            DataContext = vm;
        }

        private LoginViewModel vm;
        private View.Register register = new View.Register();
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelLogin.password = Password.Password;
            await vm.CekLogin();
            if (vm.StatusLogin == true)
            {
                App.ViewRouting(false);
                App.ViewRouting(true, new UserControl_Order1());
                Close();
            }
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            register.Show();
            Close();
        }
    }
}
