using Cafe_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cafe_App.View
{
    /// <summary>
    /// Interaction logic for UserControl_Laporan.xaml
    /// </summary>
    public partial class UserControl_Laporan : UserControl
    {
        public UserControl_Laporan()
        {
            InitializeComponent();
            vm = new LaporanViewModel();
            DataContext = vm;
        }

        private LaporanViewModel vm;

        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(5);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_DetailJual(vm));
        }

        private async void ListView_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(5);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_DetailBeli(vm));
        }
    }
}
