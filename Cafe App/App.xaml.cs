using Cafe_App.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Cafe_App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Dashboard View { get; set; }
        public static View.SplashScreen splashScreen { get; set; }
        public static void ViewRouting(bool flag, Control content = null)
        {
            if (View == null)
            {
                View = new Dashboard();
                View.Show();
            }
            if (flag == true)
            {
                View.PanelMenu.Children.Add(content);
            }
            else
            {
                View.PanelMenu.Children.Clear();
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            splashScreen = new View.SplashScreen();
            splashScreen.Show();
        }
    }
}
